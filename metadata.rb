version '0.3.0'

name 'github'
maintainer 'Serge A. Salamanka'
maintainer_email 'salamanka.serge@gmail.com'
license 'Apache v2.0'
description 'Installs/Configures github'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://github.com/skif-software/github-cookbook'
issues_url 'https://github.com/skif-software/github-cookbook/issues'

chef_version '>= 12.14' if respond_to?(:chef_version)
