# github CHANGELOG

This file is used to list changes made in each version of the github cookbook.

# 0.3.1

- Add dryrun functionality (wip)

# 0.3.0

- Add repository delete action
- Add reference resource
- Fix organization membership resource when user already invited
- Fix team member resource to include user when invitation is pending

# 0.2.0

- Add debug property
- Modify team resource
- Add organization_membership resource
- Add team_member resource
- Add team_repository resource

# 0.1.0

Initial release.
