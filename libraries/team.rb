#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# https://help.github.com/articles/about-teams/
# https://developer.github.com/v3/teams/
# http://octokit.github.io/octokit.rb/Octokit/Client/Organizations.html

module GitHubCookbook
  class Team < Chef::Resource
    resource_name :github_team

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: true
    property :team,         String, required: false, name_property: true
    property :parent_team,  String, required: false, default: ''
    property :options,      Hash,   required: true
    property :debug,  [true, false], required: false, default: false
    property :dryrun, [true, false], required: false, default: false
    property :action_response, required: false
    property :action_changes,  required: false

    default_action :create

    require 'octokit'

    include GitHubCookbook::Helper

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def get_team_id(team_name)
      teams = client.organization_teams(organization)
      team_id = nil
      teams.each do |t|
        if t[:name] == team_name
          team_id = t[:id]
        end
      end
      return team_id
    end

    def current_state
      client.team(get_team_id(team))
    end

    def expected_state
      options
    end

    def team_exists?
      teams = client.organization_teams(organization)
      team_exists = false
      teams.each do |t|
        if t[:name] == options[:name]
          team_exists = true
        end
      end
      return team_exists
    end

    action :create do
      if team_exists?
        # TODO
      else
        if new_resource.parent_team.empty?
          new_resource.action_response = client.create_team(new_resource.organization,
                                                            new_resource.options.dup)
        else
          options_with_parent_team_id = new_resource.options.dup
          options_with_parent_team_id[:parent_team_id] = get_team_id(new_resource.parent_team)
          new_resource.action_response = client.create_team(new_resource.organization,
                                                            options_with_parent_team_id)
        end
        ::Chef::Log.info("#{new_resource.action_response.to_h.to_s}") if new_resource.debug
      end
    end

    action :update do
      if new_resource.dryrun
        new_resource.action_changes = compute_changes(current_state, expected_state)
        log_changes new_resource.action_changes
      else
        if new_resource.parent_team.empty?
          new_resource.action_response = client.update_team(get_team_id(new_resource.team),
                                                            new_resource.options.dup)
        else
          options_with_parent_team_id = new_resource.options.dup
          options_with_parent_team_id[:parent_team_id] = get_team_id(new_resource.parent_team)
          new_resource.action_response = client.update_team(get_team_id(new_resource.team),
                                                            options_with_parent_team_id)
        end
      end
      ::Chef::Log.info("#{new_resource.action_response.to_h.to_s}") if new_resource.debug
    end

    action :list_child_teams do
        new_resource.action_response = client.child_teams(get_team_id(new_resource.team),
                                                          :accept => "application/vnd.github.hellcat-preview+json")
        ::Chef::Log.info("#{new_resource.action_response.to_h.to_s}") if new_resource.debug
    end

  end # class
end # module
