#
# Cookbook:: github
# Library:: helper
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  module Helper
    require 'hashdiff'
    require "awesome_print"

    def compute_changes(current, expected)
      expected_keys = expected.transform_keys(&:to_sym).keys
      current_hash  = current.reject {|k,v| !expected_keys.include?(k)}
      expected_hash = expected
      diff = HashDiff.diff(current_hash, expected_hash.transform_keys(&:to_sym))
      return diff
    end

    def no_changes
      return false
    end

    def log_changes(changes)
      changes.each do |ch|
          case ch[0]
          when "~"
            ::Chef::Log.info( ch.to_s.red )
          when "-"
            ::Chef::Log.info( ch.to_s.red )
          when "+"
            ::Chef::Log.info( ch.to_s.yellow )
          when "WARNING"
            ::Chef::Log.info( ch.to_s.red )
          when "NEW"
            ::Chef::Log.info( ch.to_s.green )
          end
      end
    end

    def log(message)
      ::Chef::Log.info( response )
    end
  end
end
