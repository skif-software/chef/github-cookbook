#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  class OrganizationMembership < Chef::Resource
    resource_name :github_organization_membership

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: false
    property :member,       String, required: false, name_property: true
    property :options,      Hash,   required: false
    property :debug, [true, false], required: false, default: false
    property :action_response, required: false

    default_action :update

    require 'octokit'

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def user_invited?
      invitations = client.organization_invitations(organization)
      invitation_exists = false
      invitations.each do |i|
        if i[:login] == options[:user]
          invitation_exists = true
        end
      end
      return invitation_exists
    end

    action :update do
      if user_invited?
        # TODO: return
      else
        new_resource.action_response = client.update_organization_membership(new_resource.organization, new_resource.options.dup)
        ::Chef::Log.info(new_resource.action_response.to_h.to_s) if new_resource.debug
      end
    end

    action :remove do
      new_resource.action_response = client.remove_organization_membership(new_resource.organization, new_resource.options.dup)
      ::Chef::Log.info(new_resource.action_response.to_s) if new_resource.debug
    end

    action :get do
      new_resource.action_response = client.organization_membership(new_resource.organization, new_resource.options.dup)
      ::Chef::Log.info(new_resource.action_response.to_h.to_s) if new_resource.debug
    end

  end # class
end # module
