#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  class Organization < Chef::Resource
    resource_name :github_organization

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: false, name_property: true
    property :options,      Hash,   required: false
    property :debug,  [true, false], required: false, default: false
    property :dryrun, [true, false], required: false, default: false
    property :action_response, required: false
    property :action_changes,  required: false

    default_action :update

    require 'octokit'

    include GitHubCookbook::Helper

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def current_state
      client.organization(organization).to_h
    end

    def expected_state
      options
    end

    action :update do
      if new_resource.dryrun
        new_resource.action_changes = compute_changes(current_state, expected_state)
        log_changes new_resource.action_changes
      else
        new_resource.action_response = client.update_organization(new_resource.organization, new_resource.options.dup)
        log(new_resource.action_response.to_h.to_s) if new_resource.debug
      end
    end

    action :get do
      new_resource.action_response = current_state
      log(new_resource.action_response.to_h.to_s) if new_resource.debug
      new_resource.action_changes = no_changes
      log("no_changes") if new_resource.dryrun
    end

  end # class
end # module
