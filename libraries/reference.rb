#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# https://developer.github.com/v3/git/refs/
# http://octokit.github.io/octokit.rb/Octokit/Client/Refs.html
# https://stackoverflow.com/questions/9506181/github-api-create-branch
# https://gist.github.com/potherca/3964930

module GitHubCookbook
  class Reference < Chef::Resource
    resource_name :github_reference

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: false
    property :repository,   String, required: true
    property :reference,    String, required: false, name_property: true
    property :base_reference, String, required: false, default: 'heads/master'
    property :sha,          String, required: false, default: ''
    property :debug, [true, false], required: false, default: false
    property :action_response, required: false

    default_action :create

    require 'octokit'

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def get_sha
      refs = client.refs(repository)
      latest_sha = nil
      refs.each do |r|
        if r[:ref] == "refs/" + base_reference
          latest_sha = r[:object][:sha]
        end
      end
      return latest_sha
    end

    def ref_exists?
      refs = client.refs(repository)
      ref_exists = false
      refs.each do |r|
        if r[:ref] == "refs/" + reference
          ref_exists = true
        end
      end
      return ref_exists
    end

    action :create do
      if ref_exists?
        # TODO
      else
        if new_resource.sha.empty?
          sha = get_sha
        end
        new_resource.action_response = client.create_ref(new_resource.repository,
                                                            new_resource.reference,
                                                            sha)
        ::Chef::Log.info("#{new_resource.action_response.to_h.to_s}") if new_resource.debug
      end
    end

  end # class
end # module
