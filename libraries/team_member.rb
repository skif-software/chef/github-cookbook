#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  class TeamMember < Chef::Resource
    resource_name :github_team_member

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: true
    property :team,         String, required: true
    property :member,       String, required: false, name_property: true
    property :debug, [true, false], required: false, default: false
    property :action_response, required: false

    default_action :add

    require 'octokit'

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def get_team_id
      teams = client.organization_teams(organization)
      team_id = nil
      teams.each do |t|
        if t[:name] == team
          team_id = t[:id]
        end
      end
      return team_id
    end

    def member_exists?
      members = client.organization_members(organization)
      member_exists = false
      members.each do |m|
        if m[:login] == member
          member_exists = true
        end
      end
      return member_exists
    end

    action :add do
      if member_exists?
        new_resource.action_response = client.add_team_member(get_team_id, new_resource.member)
        ::Chef::Log.info(new_resource.action_response.to_s) if new_resource.debug
      else
        # In case the invitation to organization was already sent but not yet accepted
        # we also send an invitation to the team
        new_resource.action_response = client.add_team_membership(get_team_id, new_resource.member)
        ::Chef::Log.info(new_resource.action_response.to_h.to_s) if new_resource.debug
      end
    end

    action :remove do
        new_resource.action_response = client.remove_team_member(get_team_id, new_resource.member)
        ::Chef::Log.info(new_resource.action_response.to_s) if new_resource.debug
    end

  end # class
end # module
