#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>
#
# https://help.github.com/articles/managing-access-to-your-organization-s-repositories/
# https://help.github.com/articles/repository-permission-levels-for-an-organization/

module GitHubCookbook
  class Repository < Chef::Resource
    resource_name :github_repository

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: true
    property :repository,         String, required: false, name_property: true
    property :options,      Hash,   required: true
    property :debug,  [true, false], required: false, default: false
    property :dryrun, [true, false], required: false, default: false
    property :action_response, required: false
    property :action_changes,  required: false

    default_action :create

    require 'octokit'

    include GitHubCookbook::Helper

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def current_state
      # organization is a different hash object in the returned JSON thus we remove it
      client.repository(options[:full_name]).to_h.tap {|h| h.delete(:organization)}
    end

    def expected_state
      # auto_init and organization options are not used in the object state
      options.dup.tap do |h|
        h.delete(:auto_init)
        h.delete(:organization)
      end
    end

    def repository_exists?
      # client.auto_paginate = true # https://github.com/octokit/octokit.rb#auto-pagination
      # repositories = client.organization_repositories(organization)
      # # # Custom pagination alternative to 'client.auto_paginate':
      # # last_response = client.last_response
      # # until last_response.rels[:next].nil?
      # #   last_response = last_response.rels[:next].get
      # #   repositories.concat last_response.data
      # # end # https://developer.github.com/v3/guides/traversing-with-pagination/
      # repository_exists = false
      # repositories.each do |r|
      #   if r[:full_name] == options[:full_name]
      #     repository_exists = true
      #   end
      # end
      # return repository_exists
      client.repository?(options[:full_name])
    end

    def repository_list
       client.auto_paginate = true # https://github.com/octokit/octokit.rb#auto-pagination
       repositories = client.organization_repositories(organization)
       return repositories.to_a
    end

    action :create do
      if new_resource.dryrun
        repository_exists? ? log("no_changes") : log_changes([["NEW", "repository #{new_resource.options[:full_name]} will be created"]])
      else
        if repository_exists?
          # TODO: return it exists
        else
          new_resource.action_response = client.create_repository(new_resource.options[:name], new_resource.options.dup)
        end
      end
      log(new_resource.action_response.to_h.to_s) if new_resource.debug
    end

    action :update do
      if new_resource.dryrun
        new_resource.action_changes = compute_changes(current_state, expected_state)
        log_changes new_resource.action_changes
      else
        new_resource.action_response = client.edit_repository(new_resource.options[:full_name], new_resource.options.dup)
        log(new_resource.action_response.to_h.to_s) if new_resource.debug
      end
    end

    action :delete do
      if new_resource.dryrun
        repository_exists? ? log_changes([["WARNING", "repository #{new_resource.options[:full_name]} will be deleted"]]) : log("no_changes")
      else
        if repository_exists?
          new_resource.action_response = client.delete_repository(new_resource.options[:full_name])
        else
          # TODO: return if doesn't exist
        end
      end
      log(new_resource.action_response.to_s) if new_resource.debug
    end

    action :list do
      new_resource.action_response = repository_list
      log(new_resource.action_response.to_s) if new_resource.debug
    end

  end # class
end # module
