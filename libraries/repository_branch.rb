#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  class RepositoryBranch < Chef::Resource
    resource_name :github_repository_branch

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: true
    property :repository,         String, required: true
    property :branch,         String, required: false, name_property: true
    property :options,      Hash,   required: true
    property :debug, [true, false], required: false, default: false
    property :action_response, required: false

    default_action :protect

    require 'octokit'

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def repository_branch_exists?
      repository_branches = client.branches(repository)
      repository_branch_exists = false
      repository_branches.each do |b|
        if b[:name] == branch
          repository_branch_exists = true
        end
      end
      return repository_branch_exists
    end

    action :protect do
      if repository_branch_exists?
        new_resource.action_response = client.protect_branch(new_resource.repository, new_resource.branch, new_resource.options.dup)
      else
        # TODO: return it doesn't exist
      end
      ::Chef::Log.info(new_resource.action_response.to_h.to_s) if new_resource.debug
    end

  end # class
end # module
