#
# Cookbook:: github
# Library:: api
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  class API < Chef::Resource
    resource_name :github_api
    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :api_method,   String, required: false
    property :arguments,    Array,  required: false, default: []
    property :options,      Hash,   required: false
    property :response,             required: false

    default_action :request

    require 'octokit'

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    action :request do
      if property_is_set?(:options)
        new_resource.response = client.send(new_resource.api_method, *new_resource.arguments, new_resource.options)
      else
        new_resource.response = client.send(new_resource.api_method, *new_resource.arguments)
      end
    end
  end
end
