#
# Cookbook:: github
# Resource:: organization
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

module GitHubCookbook
  class TeamRepository < Chef::Resource
    resource_name :github_team_repository

    # https://github.com/octokit/octokit.net/issues/1775
    property :access_token, String, required: true, sensitive: true, desired_state: false
    property :organization, String, required: true
    property :repository,   String, required: false, name_property: true
    property :team,         String, required: true
    property :options,      Hash,   required: true
    property :debug,  [true, false], required: false, default: false
    property :dryrun, [true, false], required: false, default: false
    property :action_response, required: false
    property :action_changes, required: false

    default_action :add

    require 'octokit'

    def client
      @client ||= ::Octokit::Client.new(:access_token => access_token)
    end

    def get_team_id
      teams = client.organization_teams(organization)
      team_id = nil
      teams.each do |t|
        if t[:name] == team
          team_id = t[:id]
        end
      end
      return team_id
    end

    def get_current_state
      # make a request to the API endpoint and save into local variable
      # find out whether the object exists already
      # if the object exists then get the current state of the object
      # if the object doesn't exist then output no state

    end

    def compute_changes(current_state, expected_state)
      # calculate the difference
    end

    action :add do
        new_resource.action_response = client.add_team_repository(get_team_id,
                                                                  new_resource.repository,
                                                                  new_resource.options.dup)
        ::Chef::Log.info(new_resource.action_response.to_s) if new_resource.debug
        ::Chef::Log.info(new_resource.action_changes) if new_resource.dryrun

    end

    action :remove do
        new_resource.action_response = client.remove_team_repository(get_team_id,
                                                                  new_resource.repository)
        ::Chef::Log.info(new_resource.action_response.to_s) if new_resource.debug
    end

  end # class
end # module
