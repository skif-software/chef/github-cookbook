#
# Cookbook:: github
# Recipe:: default
# Author:: Serge A. Salamanka <salamanka.serge@gmail.com>

chef_gem 'octokit'

chef_gem 'hashdiff'

chef_gem 'awesome_print'
